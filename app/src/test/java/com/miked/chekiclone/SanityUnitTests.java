package com.miked.chekiclone;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Test;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import static org.junit.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class SanityUnitTests {

  private class TestDate {
    Date createdAt;
  }

  @Test
  public void date_formatting() throws Exception {
    Gson gson = new GsonBuilder()
        .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
        .create();

    TestDate date = gson.fromJson("{\"created_at\": \"2016-05-21T10:16:55+0000\"}", TestDate.class);
    Calendar expectedTime = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
    expectedTime.set(2016,4,21,10,16,55);

    assertEquals(expectedTime.getTime().toString(), date.createdAt.toString());
  }

  @Test
  public void price_formatting() throws Exception {
    int price = 3800_000;
    String out = String.format("%,.0f",((float)price)/1000);
    assertEquals("3,800", out);
  }
}
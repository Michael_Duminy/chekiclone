package com.miked.chekiclone;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.miked.chekiclone.fragments.SearchFragment;
import com.miked.chekiclone.fragments.VehicleListFragment;

import java.lang.ref.WeakReference;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements SearchFragment.SearchClickListener {

  @BindView(R.id.container)
  ViewPager mViewPager;

  @BindView(R.id.tabs)
  TabLayout mTabLayout;

  @BindView(R.id.fab)
  FloatingActionButton fab;


  private SectionsPagerAdapter mSectionsPagerAdapter;
  private boolean searchInProgress;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);

    mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

    mViewPager.setAdapter(mSectionsPagerAdapter);
    mViewPager.setOffscreenPageLimit(mSectionsPagerAdapter.getCount() - 1);

    mTabLayout.setupWithViewPager(mViewPager, true);

    mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
      @Override
      public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
      }

      @Override
      public void onPageSelected(int position) {
        switch (position) {
          case 0:
            initialFabSetup();
            break;
          case 1:
            if (searchInProgress) {
              fab.setImageResource(R.drawable.ic_close_white_24dp);
              fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  searchInProgress = false;
                  fab.hide();
                }
              });
              fab.show();
            } else {
              fab.hide();
            }
            break;
          default:
            fab.hide();
        }
      }

      @Override
      public void onPageScrollStateChanged(int state) {
      }
    });

    initialFabSetup();
  }

  private void initialFabSetup() {
    fab.setImageResource(R.drawable.ic_youtube_searched_for_white_24dp);
    fab.show();
    fab.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {

      }
    });
  }

  @Override
  public void OnSearchClicked(String make, String model) {
    searchInProgress = true;

    // move to browse screen
    mViewPager.setCurrentItem(1, true);

    VehicleListFragment fragment = (VehicleListFragment) mSectionsPagerAdapter.getItem(1);
    fragment.FilterList(make, model);
  }

  public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private WeakReference<VehicleListFragment> veh;
    private WeakReference<VehicleListFragment> fav;
    private WeakReference<SearchFragment> srh;

    public SectionsPagerAdapter(FragmentManager fm) {
      super(fm);
    }

    @Override
    public Fragment getItem(int position) {
      switch (position) {
        case 0:
          return srh == null || srh.get() == null ? (srh = new WeakReference<>(SearchFragment.newInstance())).get() : srh.get();
        case 1:
          return veh == null || veh.get() == null ? (veh = new WeakReference<>(VehicleListFragment.newInstance())).get() : veh.get();
        case 2:
          return fav == null || fav.get() == null ? (fav = new WeakReference<>(VehicleListFragment.newInstance(true))).get() : fav.get();
        default:
          return null;
      }
    }

    @Override
    public int getCount() {
      return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
      switch (position) {
        case 0:
          return "Search";
        case 1:
          return "Browse";
        case 2:
          return "Favourites";
      }
      return null;
    }
  }
}

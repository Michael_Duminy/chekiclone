package com.miked.chekiclone.models.old;

import io.realm.RealmObject;
import io.realm.annotations.Index;

public class Make extends RealmObject {

  @Index
  public int id;
  public String title;

}

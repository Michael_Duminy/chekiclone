
package com.miked.chekiclone.models;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.Index;
import io.realm.annotations.PrimaryKey;

public class Listing extends RealmObject {

  @PrimaryKey
  public int id;
  public String title;
  public Date createdAt;
  public Date updatedAt;
  public Date enableAt;
  public Date disableAt;
  public String url;
  public String description;
  public String defaultImage;
  @Index
  public String make;
  @Index
  public String model;
  public String location;
  public String listingStatus;
  public String advertiser;
  public String userId;
  public int price;
  public String currencySymbol;
  public boolean isNegotiable;
  public String engineSize;
  public String duty;
  public String driveType;
  public String driveSetup;
  public String bodyType;
  public String condition;
  public String fuelType;
  public String transmission;
  public String interior;
  public String colour;
  public String doorCount;
  public int year;
  public int mileage;
  public String registration;
  public String firstname;
  public String lastname;
  public String email;
  public String mobileNumber;
  public String contactMethod;
  public boolean moneyBackGuarantee;
  public int views;
  public boolean isFavourite;



  public String getPrice() {
    if(price == 0) return currencySymbol + " unknown";

    String sprice = String.valueOf(price);
    if (sprice.endsWith("000")) {
      return String.format("%1$s %2$,.0fk", currencySymbol, ((float)price)/1000);
    } else {
      return String.format("%1$s %2$,.2fk", currencySymbol, ((float)price)/1000);
    }
  }

  public String getHeading() {
    return colour + " " + make + " " + model + ", " + year;
  }
}
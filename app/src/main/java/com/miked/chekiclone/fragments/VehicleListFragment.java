package com.miked.chekiclone.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.miked.chekiclone.R;
import com.miked.chekiclone.models.Listing;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Copyright Michael Duminy 2016/08/17.
 */
public class VehicleListFragment extends Fragment {

  private static final String TAG = VehicleListFragment.class.getSimpleName();
  private static final String FAV = "Favourites";
  private Realm realm;

  private boolean isFavourites;

  @BindView(R.id.vehicle_list)
  RecyclerView listView;

  private Unbinder unbinder;
  private RealmResults<Listing> results;

  public VehicleListFragment() {
    Log.i(TAG, "VehicleListFragment: ");
  }

  public static VehicleListFragment newInstance() {
    return newInstance(false);
  }

  public static VehicleListFragment newInstance(boolean favouritesList) {

    Bundle args = new Bundle();
    args.putBoolean(FAV, favouritesList);

    VehicleListFragment fragment = new VehicleListFragment();
    fragment.setArguments(args);
    Log.i(TAG, "newInstance: ");
    return fragment;
  }

  public void FilterList(String make, String model){
    Toast.makeText(getActivity(), "fragment: " + make + " " + model, Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    realm = Realm.getDefaultInstance();
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_list, container, false);
    unbinder = ButterKnife.bind(this, view);

    Bundle arguments = getArguments();
    if (arguments != null && arguments.get(FAV) != null) {
      isFavourites = (boolean) arguments.get(FAV);
    }

    listView.setHasFixedSize(true);
    listView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    RealmQuery<Listing> query = realm.where(Listing.class);
    if (isFavourites) {
      query = query.equalTo("isFavourite", true);
    }

    results = query.findAllAsync();

    VehicleListAdapter mVehicleListAdapter = new VehicleListAdapter(getContext(), results, true);
    listView.setAdapter(mVehicleListAdapter);

    return view;
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (realm != null && !realm.isClosed()) {
      realm.close();
    }
  }

  public class VehicleListAdapter extends RealmRecyclerViewAdapter<Listing, VehicleListAdapter.ViewHolder> {
    private final LayoutInflater inflater;

    public VehicleListAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Listing> data, boolean autoUpdate) {
      super(context, data, true);
      inflater = LayoutInflater.from(context);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

      @BindView(R.id.primary)
      public TextView primaryText;

      @BindView(R.id.secondary)
      public TextView secondaryText;

      @BindView(R.id.image)
      public ImageView imageView;

      @BindView(R.id.favourite_button)
      public ImageButton button;

      public ViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
      }
    }

    @Override
    public VehicleListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View vehicleView = inflater.inflate(R.layout.list_item, parent, false);

      return new ViewHolder(vehicleView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
      //noinspection ConstantConditions - trust in the RealmRecyclerViewAdapter
      final Listing listing = getData().get(position);
      holder.primaryText.setText(listing.getHeading());
      holder.secondaryText.setText(listing.getPrice());
      Picasso.with(context).load(listing.defaultImage).centerCrop().fit().into(holder.imageView);

      if (listing.isFavourite) {
        holder.button.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_black_24dp));
      } else {
        holder.button.setImageDrawable(getResources().getDrawable(R.drawable.ic_favorite_border_black_24dp));
      }

      holder.button.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
          realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
              Listing edit = realm.where(Listing.class).equalTo("id", listing.id).findFirst();
              if (edit != null) {
                edit.isFavourite = !edit.isFavourite;
              }
            }
          });
        }
      });
    }

  }
}

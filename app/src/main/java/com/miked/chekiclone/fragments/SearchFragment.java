package com.miked.chekiclone.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.miked.chekiclone.R;
import com.miked.chekiclone.models.Listing;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class SearchFragment extends Fragment {

  private static final String TAG = SearchFragment.class.getSimpleName();

  private Realm realm;
  private Unbinder unbinder;

  @BindView(R.id.spinner_makes)
  Spinner spinnerMakes;
  @BindView(R.id.spinner_models)
  Spinner spinnerModel;
  @BindView(R.id.search)
  Button searchButton;

  private ArrayAdapter<String> makeListAdapter;
  private ArrayAdapter<String> modelListAdapter;

  public RealmResults<Listing> makesResults;

  SearchClickListener searchClickListener;
  public interface SearchClickListener {
    void OnSearchClicked(String make, String model);
  }

  public SearchFragment() {
  }

  public static SearchFragment newInstance() {
    SearchFragment fragment = new SearchFragment();
    Bundle args = new Bundle();
    fragment.setArguments(args);
    return fragment;
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    if (context instanceof SearchClickListener) {
      searchClickListener = (SearchClickListener) context;
    } else {
      throw new ClassCastException(context.toString()
          + " must implement SearchFragment.SearchClickListener");
    }
  }

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    realm = Realm.getDefaultInstance();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_search, container, false);
    unbinder = ButterKnife.bind(this, view);

    makeListAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());
    modelListAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, new ArrayList<String>());

    makeListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    modelListAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

    spinnerMakes.setAdapter(makeListAdapter);
    spinnerModel.setAdapter(modelListAdapter);

    spinnerMakes.setPrompt("Make");
    spinnerModel.setPrompt("Model");

    makesResults = realm.where(Listing.class).distinct("make").sort("make");
    Log.i(TAG, "onCreateView: query registered");
    populateMakesAdapter(makesResults);
    makesResults.addChangeListener(new RealmChangeListener<RealmResults<Listing>>() {
      @Override
      public void onChange(RealmResults<Listing> element) {
        Log.i(TAG, "onChange: ");
        populateMakesAdapter(element);
      }
    });

    spinnerMakes.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        String make = (String) adapterView.getItemAtPosition(i);

        RealmResults<Listing> models = realm.where(Listing.class).equalTo("make", make).distinct("model").sort("model");
        ArrayList<String> list = new ArrayList<>();
        for (Listing model : models) {
          list.add(model.model);
        }

        modelListAdapter.clear();
        modelListAdapter.addAll(list);
        modelListAdapter.notifyDataSetChanged();

        if (!spinnerModel.isEnabled()) {
          spinnerModel.setEnabled(true);
        }

        // reset selection index to first in list
        spinnerModel.setSelection(0);
      }

      @Override
      public void onNothingSelected(AdapterView<?> adapterView) {
        Log.i(TAG, "onNothingSelected: ");
      }
    });

    spinnerModel.setEnabled(false);

    return view;
  }

  private void populateMakesAdapter(RealmResults<Listing> element) {
    ArrayList<String> list = new ArrayList<>();
    for (Listing l : element) {
      list.add(l.make);
    }
    if (makeListAdapter != null) {
      makeListAdapter.clear();
      makeListAdapter.addAll(list);
      makeListAdapter.notifyDataSetChanged();
    }
  }

  @OnClick(R.id.search)
  public void Search() {
    String make = (String) spinnerMakes.getSelectedItem();
    String model = (String) spinnerModel.getSelectedItem();

    searchClickListener.OnSearchClicked(make, model);
  }


  @Override
  public void onDestroyView() {
    super.onDestroyView();
    if (unbinder != null) {
      unbinder.unbind();
    }
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    if (realm != null && !realm.isClosed()) {
      realm.close();
    }
  }


}

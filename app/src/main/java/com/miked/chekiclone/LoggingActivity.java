package com.miked.chekiclone;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

/**
 * Copyright Michael Duminy, 8/21/2016.
 */
public class LoggingActivity implements Application.ActivityLifecycleCallbacks {
  @Override
  public void onActivityCreated(Activity activity, Bundle bundle) {
    Log.i(activity.getLocalClassName(), "onActivityCreated: ");
  }

  @Override
  public void onActivityStarted(Activity activity) {
    Log.i(activity.getLocalClassName(), "onActivityStarted: ");
  }

  @Override
  public void onActivityResumed(Activity activity) {
    Log.i(activity.getLocalClassName(), "onActivityResumed: ");
  }

  @Override
  public void onActivityPaused(Activity activity) {
    Log.i(activity.getLocalClassName(), "onActivityPaused: ");
  }

  @Override
  public void onActivityStopped(Activity activity) {
    Log.i(activity.getLocalClassName(), "onActivityStopped: ");
  }

  @Override
  public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    Log.i(activity.getLocalClassName(), "onActivitySaveInstanceState: ");
  }

  @Override
  public void onActivityDestroyed(Activity activity) {
    Log.i(activity.getLocalClassName(), "onActivityDestroyed: ");
  }
}

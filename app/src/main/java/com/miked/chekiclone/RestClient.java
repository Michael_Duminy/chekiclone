package com.miked.chekiclone;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.loopj.android.http.SyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

/**
 * Copyright Michael Duminy 2016/08/18.
 */
public class RestClient {

  private static SyncHttpClient client = new SyncHttpClient();

  public static void get(Context context, String url, TextHttpResponseHandler responseHandler) {
    if (isNetworkAvailable(context)) {
      client.get(url, responseHandler);
    }
  }

  private static boolean isNetworkAvailable(Context context) {
    ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
    return networkInfo != null && networkInfo.isConnectedOrConnecting();
  }
}

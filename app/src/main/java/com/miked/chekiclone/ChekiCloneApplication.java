package com.miked.chekiclone;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Copyright Michael Duminy 2016/08/18.
 */
public class ChekiCloneApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    RealmConfiguration realmConfig = new RealmConfiguration.Builder(this).build();
    Realm.deleteRealm(realmConfig);
    Realm.setDefaultConfiguration(realmConfig);

    this.registerActivityLifecycleCallbacks(new LoggingActivity());

  }


}

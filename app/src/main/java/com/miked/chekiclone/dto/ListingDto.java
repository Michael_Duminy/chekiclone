
package com.miked.chekiclone.dto;

import com.miked.chekiclone.models.Listing;
import com.mobandme.android.transformer.compiler.Mappable;
import com.mobandme.android.transformer.compiler.Mapped;
import com.mobandme.android.transformer.compiler.Parse;

import java.util.Date;

@Mappable(with = Listing.class)
public class ListingDto {

  @Mapped
  public int id;
  @Mapped
  public String title;
  @Mapped
  public Date createdAt;
  @Mapped
  public Date updatedAt;
  @Mapped
  public Date enableAt;
  @Mapped
  public Date disableAt;
  @Mapped
  public String url;
  @Mapped
  public String description;
  @Mapped
  public String defaultImage;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item make;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item model;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item location;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item listingStatus;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item advertiser;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item userId;
  @Mapped
  public int price;
  @Mapped
  public String currencySymbol;
  @Mapped
  public boolean isNegotiable;
  @Mapped
  public String engineSize;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item duty;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item driveType;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item driveSetup;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item bodyType;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item condition;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item fuelType;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item transmission;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item interior;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item colour;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item doorCount;
  @Mapped
  public int year;
  @Mapped
  public int mileage;
  @Mapped
  public String registration;
  @Mapped
  public String firstname;
  @Mapped
  public String lastname;
  @Mapped
  public String email;
  @Mapped
  public String mobileNumber;
  @Parse(originToDestinationWith = ItemToStringParser.class, destinationToOriginWith = StringToItemParser.class)
  @Mapped
  public Item contactMethod;
  @Mapped
  public boolean moneyBackGuarantee;
  @Mapped
  public int views;
}


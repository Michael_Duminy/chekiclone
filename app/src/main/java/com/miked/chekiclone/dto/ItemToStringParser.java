package com.miked.chekiclone.dto;

import com.mobandme.android.transformer.parser.AbstractParser;

public class ItemToStringParser extends AbstractParser<Item, String> {

  @Override
  protected String onParse(Item value) {
    return value != null ? value.title : "";
  }
}


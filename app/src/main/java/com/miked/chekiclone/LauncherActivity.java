package com.miked.chekiclone;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.TextHttpResponseHandler;
import com.miked.chekiclone.dto.ListingDto;
import com.miked.chekiclone.dto.ResultsWrapper;
import com.miked.chekiclone.models.Listing;
import com.mobandme.android.transformer.Transformer;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.realm.Realm;

/**
 * Copyright Michael Duminy, 8/23/2016.
 */
public class LauncherActivity extends AppCompatActivity {
  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    new DownloadDataTask().execute("http://empty-bush-3943.getsandbox.com/listings");
  }

  private class DownloadDataTask extends AsyncTask<String, Void, Void> {

    private String TAG = "DownloadDataTask";

    @Override
    protected Void doInBackground(String... strings) {
      final Realm realm = Realm.getDefaultInstance();
      long count = realm.where(Listing.class).count();
      if (count > 0) {
        realm.close();
        return null;
      }

      RestClient.get(getApplicationContext(), strings[0], new TextHttpResponseHandler() {
        @Override
        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
          Log.i(TAG, "onFailure: ");
        }

        @Override
        public void onSuccess(int statusCode, Header[] headers, String responseString) {
          Gson gson = new GsonBuilder()
              .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
              .setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
              .create();
          ResultsWrapper results = gson.fromJson(responseString, ResultsWrapper.class);
          Log.i(TAG, "onSuccess: " + results.data.size());

          Transformer transformer = new Transformer.Builder().build(ListingDto.class);

          final List<Listing> listings = new ArrayList<>();

          for (int i = 0; i < results.data.size(); i++) {
            Listing l = (Listing) transformer.transform(results.data.get(i));
            listings.add(l);
          }

          realm.beginTransaction();
          realm.insert(listings);
          realm.commitTransaction();
          realm.close();
        }
      });

      return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
      super.onPostExecute(aVoid);

      Intent mainActivityStart = new Intent(getApplicationContext(), MainActivity.class);
      startActivity(mainActivityStart);
      finish();
      overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
  }
}
